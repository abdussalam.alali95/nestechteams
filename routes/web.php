<?php

use Illuminate\Support\Facades\Route;
use Chatify\Http\Controllers\MessagesController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});
Route::resource('/users','Users\UsersController');
Route::get('/login',function(){
    return view('auth.login');
})->name('login')->middleware('guest');
Route::get('/register',function(){
   return view('auth.register');
})->name('register');

Route::post('/login','Auth\LoginController@login');
Route::post('/register','Auth\RegisterController@register');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::group(['prefix'=>'groups'],function(){
    Route::get('/test','MessagesController@getGroups');
});

Route::get('/generate',function ()
{
    $users = \App\User::all();
    foreach($users as $u)
    {
        $u->channel = \Illuminate\Support\Str::random(32);
        $u->save();
    }
    return $users;
});

Route::get('/push',function(){
   $pusher = \Chatify\Facades\ChatifyMessenger::pusher();
   foreach(\App\User::all() as $user)
   {
       $pusher->trigger($user->channel,'newmessage','hello');
   }
   return "done";
});


