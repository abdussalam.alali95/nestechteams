<?php

use Illuminate\Database\Seeder;

class DepartmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [
            'name'=>'Information Technology',
            'created_at'=>\Carbon\Carbon::now(),
            'updated_at'=>\Carbon\Carbon::now(),
        ];
        \App\Department::create($data);

        $data = [
            'name'=>'Telecommunications Engineering',
            'created_at'=>\Carbon\Carbon::now(),
            'updated_at'=>\Carbon\Carbon::now(),
        ];
        \App\Department::create($data);

        $data = [
            'name'=>'Mechanical Engineering',
            'created_at'=>\Carbon\Carbon::now(),
            'updated_at'=>\Carbon\Carbon::now(),
        ];
        \App\Department::create($data);

        $data = [
            'name'=>'Mechatronics Engineering',
            'created_at'=>\Carbon\Carbon::now(),
            'updated_at'=>\Carbon\Carbon::now(),
        ];
        \App\Department::create($data);
    }
}
