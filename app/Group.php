<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    //
    public function users()
    {
        return $this->belongsToMany('App\User','user_group');
    }
    public function messages() {
        return $this->hasMany('App\Message','to_id','id');
    }
}
