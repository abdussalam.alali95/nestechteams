<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class GroupMessageStatus extends Model
{
    // get unseen message count
    public function getCount($g) {
        $item = GroupMessageStatus::where('user_id',Auth::user()->id)->where('group_id',$g)->get()->first();
        if($item!=null)
            return $item->qty;
        return 0;
    }
    // make all messages seen
    public function makeSeen($g) {
        $item = GroupMessageStatus::where('user_id',Auth::user()->id)->where('group_id',$g)->get()->first();
        if($item!=null)
        {
            $item->qty = 0;
            $item->save();
        }
    }
    // increase counter
    public function incCount($g)
    {
        $gr = Group::find($g);
        foreach($gr->users()->get() as $user)
        {
            if(Auth::user()->id == $user->id) continue;
            $item = GroupMessageStatus::where('user_id',$user->id)->where('group_id',$g)->get()->first();
            if($item == null)
            {
                $item = new GroupMessageStatus();
                $item->user_id = $user->id;
                $item->group_id = $g;
                $item->qty = 0;
            }
            $item->qty = $item->qty+1;
            $item->save();
        }
    }
}
