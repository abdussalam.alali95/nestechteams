<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class TestController extends Controller
{
    //
   public function __construct(){
       return $this->middleware('auth');
   }
   public function index() {
       $user = User::all();
       return view('users.index',compact('users'));
   }
   public function create() {
       return view('users.create');
   }
   public function show($id) {
       $user = User::find($id);
       return view('users.edit',compact('user'));
   }
   public function edit($id) {
       $user = User::find($id);
       return view('users.edit',compact('user'));
   }
   public function update(Request  $request,$id)
   {
       $user = User::find($id);
       $user->name = $request->name;
       $user->email = $request->email;
       $user->password = Hash::make($request->password);
       $user->avatar = $request->avatar;
       $user->save();
   }
   public function store(Request $request) {
       $user = new User();
       $user->name = $request->name;
       $user->email = $request->email;
       $user->password = Hash::make($request->password);
       $user->avatar = $request->avatar;
       $user->channel = Str::random(32);
       $user->save();


       return redirect()->back()->with(['success'=>'user created successfully']);
   }

   public function destroy($id)
   {
       $user = User::find($id);
       $user->delete();

       return redirect()->back()->with(['success'=>'user deleted successfully!']);
   }

   public function login(Request $request) {
       $creds = $request->only(['email','password']);
       if(Auth::attempt($creds))
       {
           return response()->json(['success'=>true,'msg'=>'user authenticated']);
       }
       return response()->json(['success'=>false,'msg'=>'error in username or password']);
   }

}
