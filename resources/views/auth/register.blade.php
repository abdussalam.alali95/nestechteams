<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Nestech Teams</title>
    <meta name="description" content="#">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap core CSS -->
    <link href="dist/css/lib/bootstrap.min.css" type="text/css" rel="stylesheet">
    <!-- Swipe core CSS -->
    <link href="dist/css/swipe.min.css" type="text/css" rel="stylesheet">
    <!-- Favicon -->
    <link href="dist/img/favicon.png" type="image/png" rel="icon">
    <style>
        .error{
            color:#ff0000;
        }
    </style>
</head>
<body class="start">
<main>
    <div class="layout">
        <!-- Start of Sign Up -->
        <div class="main order-md-2">
            <div class="start">
                <div class="container">
                    <div class="col-md-12">
                        <div class="content">
                            <h1>انشاء حساب</h1>
                            <div class="third-party">
                                <button class="btn item bg-blue">
                                    <i class="material-icons">chat</i>
                                </button>
                                <button class="btn item bg-teal">
                                    <i class="material-icons">voice_chat</i>
                                </button>
                                <button class="btn item bg-purple">
                                    <i class="material-icons">videocam</i>
                                </button>
                            </div>
                            <p>المعلومات الشخصية</p>
                            <form class="signup" action="/register" method="post">
                                @csrf
                                <div class="form-parent">
                                    <div class="form-group">
                                        <input type="text" id="inputName" name="name" class="form-control" placeholder="اسم المستخدم" required>
                                        <button class="btn icon"><i class="material-icons">person_outline</i></button>
                                        <span class="error">
                                            @error('name')
                                                {{ $message }}
                                            @enderror
                                        </span>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" id="inputEmail" name="email" class="form-control" placeholder="البريد الالكتروني" required>
                                        <button class="btn icon"><i class="material-icons">mail_outline</i></button>
                                        <span class="error">
                                            @error('email')
                                            {{ $message }}
                                            @enderror
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="password" id="inputPassword" name="password" class="form-control" placeholder="كلمة المرور" required>
                                    <button class="btn icon"><i class="material-icons">lock_outline</i></button>
                                    <span class="error">
                                        @error('password')
                                        {{ $message }}
                                        @enderror
                                        </span>
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password_confirmation" id="inputPassword" class="form-control" placeholder="تأكيد كلمة المرور" required>
                                    <button class="btn icon"><i class="material-icons">lock_outline</i></button>
                                    <span class="error">
                                        @error('password_confirmation')
                                        {{ $message }}
                                        @enderror
                                        </span>
                                </div>
                                <button type="submit" class="btn button">تسجيل </button>
                                <div class="callout">
                                    <span>لديك حساب مسبقا؟<a href="/login">تسجيل دخول</a></span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Sign Up -->
        <!-- Start of Sidebar -->
        <div class="aside order-md-1">
            <div class="container">
                <div class="col-md-12">
                    <div class="preference">
                        <h2>لديك حساب مسبقا؟</h2>
                        <p>قم بتسجيل الدخول</p>
                        <a href="/login" class="btn button">تسجيل الدخول</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Sidebar -->
    </div> <!-- Layout -->
</main>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="dist/js/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="dist/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="dist/js/vendor/popper.min.js"></script>
<script src="dist/js/bootstrap.min.js"></script>
</body>


</html>
