<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>تسجيل الدخول</title>
    <meta name="description" content="#">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap core CSS -->
    <link href="/dist/css/lib/bootstrap.min.css" type="text/css" rel="stylesheet">
    <!-- Swipe core CSS -->
    <link href="/dist/css/swipe.min.css" type="text/css" rel="stylesheet">
    <!-- Favicon -->
    <link href="/dist/img/favicon.png" type="image/png" rel="icon">
    <style>
        .error  {
            color: #ff0000;
        }
    </style>
</head>
<body class="start">
<main>
    <div class="layout">
        <!-- Start of Sign In -->
        <div class="main order-md-1">
            <div class="start">
                <div class="container">
                    <div class="col-md-12">
                        <div class="content">
                            <h1>تسجيل الدخول</h1>
                            <div class="third-party">
                                <button class="btn item bg-blue">
                                    <i class="material-icons">chat</i>
                                </button>
                                <button class="btn item bg-teal">
                                    <i class="material-icons">voice_chat</i>
                                </button>
                                <button class="btn item bg-purple">
                                    <i class="material-icons">videocam</i>
                                </button>
                            </div>
                            <p></p>
                            <form action="/login" method="post">
                                @csrf
                                <div class="form-group">
                                    <input type="email" name="email" id="inputEmail" class="form-control" placeholder="البريد الالكتروني" required>
                                    <button class="btn icon"><i class="material-icons">mail_outline</i></button>
                                    <span class="error">
                                        @error('email')
                                            {{ $message }}
                                        @enderror
                                    </span>
                                </div>

                                <div class="form-group">
                                    <input type="password" name="password" id="inputPassword" class="form-control" placeholder="كلمة المرور" required>
                                    <button class="btn icon"><i class="material-icons">lock_outline</i></button>
                                    <span class="error">
                                        @error('password')
                                        {{ $message }}
                                        @enderror
                                    </span>
                                </div>
                                <button type="submit" class="btn button">تسجيل الدخول</button>
                                <div class="callout">
                                    <span>لا تملك حساب? <a href="/register">إنشاء حساب</a></span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="aside order-md-2">
            <div class="container">
                <div class="col-md-12">
                    <div class="preference">
                        <h2>مرحبا بك في Nestech Teams</h2>
                        <p>يمكنك الاستفادة من التطبيق عن طريق تسجيل دخولك أو انشاء حساب جديد</p>
                        <a href="/register" class="btn button">تسجيل حساب جديد</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script src="/dist/js/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/dist/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="/dist/js/vendor/popper.min.js"></script>
<script src="/dist/js/bootstrap.min.js"></script>
</body>

</html>
