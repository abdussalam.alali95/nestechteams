@extends('layouts.app')

@section('content')

    <div class="container-fluid">

        <div class="row my-3">
            <div class="col-md-4">
                <div class="card text-center">
                    <div class="card-block">
                        <h4 class="card-title">NestChat</h4>
                        <h2><i class="fa fa-comment-alt fa-3x"></i></h2>
                    </div>
                    <div class="row px-2 no-gutters">
                        <div class="col-12">
                            <a href="/nestchat" target="_blank"><button class="btn btn-outline-primary">Open</button></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card text-center">
                    <div class="card-block">
                        <h4 class="card-title">NestProjects</h4>
                        <h2><i class="fa fa-tasks fa-3x"></i></h2>
                    </div>
                    <div class="row px-2 no-gutters">
                        <div class="col-12">
                            <a href="{{env('PM_URL')}}" target="_blank"><button class="btn btn-outline-primary">Open</button></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card text-center">
                    <div class="card-block">
                        <h4 class="card-title">NestArchive</h4>
                        <h2><i class="fa fa-file-word fa-3x"></i></h2>
                    </div>
                    <div class="row px-2 no-gutters">
                        <div class="col-12">
                            <a href="{{env('PYDIO_URL')}}" target="_blank"> <button class="btn btn-outline-primary">Open</button></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card text-center">
                    <div class="card-block">
                        <h4 class="card-title">NestLab</h4>
                        <h2><i class="fa fa-code-branch fa-3x"></i></h2>
                    </div>
                    <div class="row px-2 no-gutters">
                        <div class="col-12">
                            <a href="{{env('GITLAB_URL')}}" target="_blank"> <button class="btn btn-outline-primary">Open</button></a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <div class="card text-center">
                    <div class="card-block">
                        <h4 class="card-title">Nest-Requests</h4>
                        <h2><i class="fa fa-envelope-open-text fa-3x"></i></h2>
                    </div>
                    <div class="row px-2 no-gutters">
                        <div class="col-12">
                            <a href="{{env('REQUESTS_URL')}}" target="_blank"><button class="btn btn-outline-primary">Open</button></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card text-center">
                    <div class="card-block">
                        <h4 class="card-title">NestReports</h4>
                        <h2><i class="fa fa-stopwatch-20 fa-3x"></i></h2>
                    </div>
                    <div class="row px-2 no-gutters">
                        <div class="col-12">
                            <a href="{{env('REPORTS_URL')}}" target="_blank"><button class="btn btn-outline-primary">Open</button></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card text-center">
                    <div class="card-block">
                        <h4 class="card-title">Nestofiy</h4>
                        <h2><i class="fa fa-bell fa-3x"></i></h2>
                    </div>
                    <div class="row px-2 no-gutters">
                        <div class="col-12">
                            <button class="btn btn-outline-primary" >Browser extension</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card text-center">
                    <div class="card-block">
                        <h4 class="card-title">NestUsers</h4>
                        <h2><i class="fa fa-users-cog fa-3x"></i></h2>
                    </div>
                    <div class="row px-2 no-gutters">
                        <div class="col-12">
                            <button class="btn btn-outline-primary">Open</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="card text-center">
                    <div class="card-block">
                        <h4 class="card-title">NestGroups</h4>
                        <h2><i class="fa fa-users fa-3x"></i></h2>
                    </div>
                    <div class="row px-2 no-gutters">
                        <div class="col-12">
                            <button class="btn btn-outline-primary">Open</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        setTimeout(pingPong,1800);
        function pingPong()
        {
            $.ajax({
               url:'/trackTime',

            });
        }
    </script>
@endsection
