## pusher credintals :
in .env file fill the credentials : <br>
`PUSHER_APP_ID=1065710`<br>
 `PUSHER_APP_KEY=61a51baffe0676b066b5`<br>
 `PUSHER_APP_SECRET=5120cfa80aa41614fa2e`<br>
`PUSHER_APP_CLUSTER=eu` `<br>

## Setup 
### migrate the DB tables :<br>
`php artisan migrate`

### install node modules 
`npm run install`

### run production or development
`npm run dev`

### run laravel app 
`php artisan serve`

### register new user 
`/register`
after registration you'll redirected to `'/nestech'` which is an instance of chat app 
